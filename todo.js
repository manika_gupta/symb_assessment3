onload = newElement;

function newElement() {
    let task = document.getElementById("task");
    let category = document.getElementById("category");
    let addbtn = document.getElementById("addbtn");


    addbtn.addEventListener("click", additems, false);
    function additems() {
        if (task.value == "" || category.value == "")
            alert("you must add some task and category")
        else {
            let task_value = task.value;

            let category_value = category.value;


            let tab = document.getElementById("table1");
            let r = document.createElement("tr");
            r.className = "r"
            tab.appendChild(r);

            check = document.createElement("input")
            check.type = "checkbox";
            check.addEventListener("click", function () {
                r.classList.toggle("strike");
            }, false)
            col1 = document.createElement("td")
            col1.className = "td"
            col1.align = "center"
            col1.appendChild(check)
            r.appendChild(col1);

            col2 = document.createElement("td")
            col2.innerText = task.value;
            col2.align = "center"
            r.appendChild(col2)

            col3 = document.createElement("td")
            col3.innerText = category.value;
            col3.align = "center"
            r.appendChild(col3);

            del = document.createElement("span")
            del.innerText = "x"
            del.addEventListener("click", function () {
                r.remove();
            }, false)
            col4 = document.createElement("td")
            col4.align = "center"
            col4.appendChild(del)
            r.appendChild(col4)

            task.value = "";
            category.value = "";

        }
    }
}

function removeAll() {
    document.querySelector(".table").innerText = "";
}
